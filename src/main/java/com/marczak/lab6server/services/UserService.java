package com.marczak.lab6server.services;

import com.marczak.lab6server.domain.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
    void save(User user);
    Optional<User> findByLoginAndPassword(String login, String password);
    Optional<User> findByToken(String token);
    List<User> findAll();
    Optional<User> findById(int id);
}
