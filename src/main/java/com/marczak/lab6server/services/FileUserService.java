package com.marczak.lab6server.services;

import com.marczak.lab6server.domain.User;
import org.springframework.stereotype.Service;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
public class FileUserService implements UserService {

    private static final String FILENAME = "Lab06Database";
    private List<User> userList;

    public FileUserService() {
        load();

        if (userList.size() == 0)
            initializeUserList();
    }

    @Override
    public void save(User user) {
        if (user.getId() != 0) {
            userList.stream()
                    .filter(u -> u.getId() == user.getId())
                    .forEach(u -> userList.remove(u));
        } else {
            userList.stream()
                    .max(Comparator.comparing(User::getId))
                    .ifPresent(u -> user.setId(u.getId() + 1));
        }
        userList.add(user);
        save();
    }

    @Override
    public Optional<User> findByLoginAndPassword(String login, String password) {
        return userList.stream()
                .filter(u -> u.getLogin().equals(login) && u.getPassword().equals(password))
                .findFirst();
    }

    @Override
    public Optional<User> findByToken(String token) {
        return userList.stream()
                .filter(u -> u.getToken().equals(token))
                .findFirst();
    }

    @Override
    public List<User> findAll() {
        return userList;
    }

    @Override
    public Optional<User> findById(int id) {
        return userList.stream()
                .filter(u -> u.getId() == id)
                .findFirst();
    }

    private void save() {
        try (
                ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(FILENAME));
        ) {
            out.writeObject(userList);
        } catch (Exception ignore) {}
    }

    private void load() {
        try (
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(FILENAME));
        ) {
            userList = (List<User>) in.readObject();
        } catch (Exception x) {
            userList = new ArrayList<>();
        }
    }

    private void initializeUserList() {
        userList.add(new User(1, "kamil", "skar", ""));
    }
}
