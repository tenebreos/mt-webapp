package com.marczak.lab6server.controllers;

import com.marczak.lab6server.domain.Debtor;
import com.marczak.lab6server.domain.User;
import com.marczak.lab6server.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.FileInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@RequestMapping("/MSK")
public class MSKController {

    private static final String DEBTOR_PHOTOS_DIR = "DebtorPhotos";
    private static final String ANDROID_IMAGE_PATH = "src/main/resources/static/android.png";
    private final UserService userService;

    @Autowired
    public MSKController(UserService userService) {
        this.userService = userService;
    }

    // TODO: please, for the love of god, rewrite error handling to something consistent and sane.
    //  Right now we are returning errors as 200 OK. This API is a hall of shame. It gives REST a bad name.

    @GetMapping("/Hello")
    public Object hello() {
        return new Object() {
            public String response = "Tu serwer uczelni, over.";
        };
    }

    @GetMapping(path = "/AndroidImage", produces = MediaType.IMAGE_PNG_VALUE)
    public byte[] androidImage() {
        try (FileInputStream fileInputStream = new FileInputStream(ANDROID_IMAGE_PATH)) {
            return fileInputStream.readAllBytes();
        } catch (Exception ignore) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(method = {GET, POST}, path = {"Authenticate", "AuthenticateGet"})
    public Object authenticate(String login, String password) {
        Optional<String> tokenOptional = checkAuthenticate(login, password);
        if (tokenOptional.isPresent()) {
            return new Object() {
                public String response = "success";
                public String token = tokenOptional.get();
            };
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }
    }

    private Optional<String> checkAuthenticate(String login, String password) {
        Optional<User> userOptional = userService.findByLoginAndPassword(login, password);
        if (userOptional.isPresent()) {
            String token = UUID.randomUUID().toString();
            userOptional.get().setToken(token);
            return Optional.of(token);
        } else {
            return Optional.empty();
        }
    }

    @GetMapping("/GetDebtors")
    public Object getDebtors(String token) {
        Optional<User> userOptional = userService.findByToken(token);
        if (userOptional.isPresent()) {
            return new Object() {
                public String response = "success";
                public List<Debtor> debtors = userOptional.get().getDebtorList();
            };
        } else {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
    }

    @GetMapping("/GetUsersData")
    public Object getUsersData() {
        return new Object() {
            public List<User> users = userService.findAll();
        };
    }

    @GetMapping("/GenerateGuidFor")
    public Object generateGuidFor(int id) {
        Optional<User> userOptional = userService.findById(id);
        if (userOptional.isPresent()) {
            User user_ = userOptional.get();
            user_.setToken(UUID.randomUUID().toString());
            return new Object() { public User user = user_; };
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(path = "/GetDebtorImage", produces = MediaType.IMAGE_PNG_VALUE)
    public byte[] getDebtorImage(String token, String photoName) {
        try (
                FileInputStream in = new FileInputStream(Paths.get(DEBTOR_PHOTOS_DIR, photoName).toFile());
        ) {
            return in.readAllBytes();
        } catch (Exception x) {
            // This part differs from the original project.
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/AddDebtor")  // Why does it use GET instead of POST?
    public Object addDebtor(String token, String debtorId, String debtorName, String debtorPhone) {
        Optional<User> userOptional = userService.findByToken(token);
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            Debtor debtor = new Debtor(debtorName, debtorId, debtorPhone);
            user.getDebtorList().add(debtor);
            return new Object() { public String response = "success"; };
        } else {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
    }

    @RequestMapping(method = { GET, POST }, path = { "/RemoveDebtor", "/DeleteDebtor" })
    public Object removeDebtor(String token, String id) {
        Optional<User> userOptional = userService.findByToken(token);
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            if (!user.getDebtorList().removeIf(e -> e.getId().equals(id))) {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND);
            }
            return new Object() { public String response = "success"; };
        } else {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
    }

    // To API wydaje się bezsensowne --- nazwy zawierają UUID.
    @PostMapping("/PostDebtorImage")
    public void postDebtorImage(String token, MultipartFile image) {
        // TODO: sure, ignore the token.
        try {
            Files.copy(image.getInputStream(), Paths.get(DEBTOR_PHOTOS_DIR)
                    .resolve(image.getOriginalFilename() != null ? image.getOriginalFilename() : "image_" + UUID.randomUUID().toString() + ".png"));
        } catch (Exception ignore) {}
    }
}
