package com.marczak.lab6server.domain;

import lombok.Data;

import java.io.Serializable;

@Data
public class Debtor implements Serializable {
    private String id;
    private String name;
    private String phone;
    private String photo;

    public Debtor(String debtorName, String debtorId, String debtorPhone) {
        this.name = debtorName;
        this.id = debtorId;
        this.phone = debtorPhone;
    }
}
