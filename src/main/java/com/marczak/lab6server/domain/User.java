package com.marczak.lab6server.domain;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
public class User implements Serializable {
    private int id;
    private String login;
    private String password;
    private String token;
    private List<Debtor> debtorList;

    public User(int id, String login, String password, String token) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.token = token;
        debtorList = new ArrayList<Debtor>();
    }
}
