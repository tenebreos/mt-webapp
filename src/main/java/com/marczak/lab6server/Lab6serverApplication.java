package com.marczak.lab6server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Lab6serverApplication {

    public static void main(String[] args) {
        SpringApplication.run(Lab6serverApplication.class, args);
    }

}
